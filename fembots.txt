 From Apple's iPhone assistant Siri to the mechanized attendants at Japan's first robot-staffed hotel, a seemingly disproportionate percentage of artificial intelligence systems have female personas. Why?

"I think there is a pattern here," said Karl Fredric MacDorman, a computer scientist and expert in human-computer interaction at Indiana University-Purdue University Indianapolis. But "I don’t know that there's one easy answer," MacDorman told Live Science.

One reason for the glut of female artificial intelligences (AIs) and androids (robots designed to look or act like humans) may be that these machines tend to perform jobs that have traditionally been associated with women. For example, many robots are designed to function as maids, personal assistants or museum guides, MacDorman said. [The 6 Strangest Robots Ever Created]

In addition, many of the engineers who design these machines are men, and "I think men find women attractive, and women are also OK dealing with women," he added.

[...]

In his own research, MacDorman studies how men and women react to voices of different genders. In one study, he and his colleagues played clips of male and female voices, and gave people a questionnaire about which voice they preferred. Then the researchers gave people a test that measured their implicit, or subconscious, preferences. The men in the study reported that they preferred female voices, but they showed no implicit preference for them, whereas the women in the study implicitly preferred female voices to male ones, even more than they admitted in the questionnaire.

"I think there's a stigma for males to prefer males, but there isn't a stigma for females to prefer females," MacDorman said.

Does the same trend toward female personas also exist among humanoid robots?

"When it comes to a disembodied voice, the chances of it being female are probably slightly higher than of it being male," said Kathleen Richardson, a social anthropologist at University College London, in England, and author of the book "An Anthropology of Robots and AI: Annihilation Anxiety and Machines" (Routledge, 2015). "But when it comes to making something fully humanoid, it's almost always male." [Super-Intelligent Machines: 7 Robotic Futures]

And when humanoid robots are female, they tend to be modeled after attractive, subservient young women, Richardson told Live Science.

[...]

Recently, Ishiguro developed a series of "Actroid" robots, manufactured by the Japanese robotics company Kokoro, for the world's first robot-staffed hotel. According to The Telegraph, the droids — which resemble young Japanese women — will act as reception attendants, waitresses, cleaners and cloakroom attendants.

Female AI personas can also be found in fiction. For example, the movie "Her" features an artificial intelligent operating system (incidentally named Samantha), who is seductively voiced by Scarlett Johansson. Her human "owner," played by Joaquin Phoenix, ends up falling in love with her.

What does this trend in creating attractive, flawless female robots say about society?

"I think that probably reflects what some men think about women — that they're not fully human beings," Richardson said. "What's necessary about them can be replicated, but when it comes to more sophisticated robots, they have to be male."

Another reason for having female robots could be that women are perceived as less threatening or more friendly than men, Richardson said. And the same could be said of childlike robots.

Hollywood's vision of robots, such as in "The Terminator" and "The Matrix" movies, makes them seem scary. "But if we designed robots to be like children, we could get people to be more comfortable with them," Richardson said. 
